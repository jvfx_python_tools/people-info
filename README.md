# People Info Command line tool

Serialize, load and output data in various formats.

## Getting Started

Source  bootstrap environment.
> source ./bootstrap.sh

List Supported formats.
> people_info --list

Save data:
> people_info -s /output/save/file.json

Load data:
> people_info -l /path/to/file/to/load.json

Output Display Data:
> people_info -o /path/to/file/to/display.rst
