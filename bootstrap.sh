
print_colour () {
    echo -e "\033[32m$1 $2"
}

print_colour 'Bootstrapping env...'

MYPWD=$PWD
print_colour 'Current Directory: ' $MYPWD

print_colour 'Setting PEOPLE_DATA env var' ''
export PEOPLE_INFO_BASE=$PWD
export PYTHONPATH=$PYTHONPATH:$PWD/python
export PATH=$PATH:$PWD/bin

env | grep PEOPLE_INFO
