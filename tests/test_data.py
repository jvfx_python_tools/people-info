import os
import sys
import subprocess
import unittest
import logging


from people_info.data_factory import DataFormatFactory

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
log.addHandler(handler)


class TestPeopleInfo(unittest.TestCase):

    def setUp(self):
        """Setup temp data paths
        """

        self.temp_path = os.path.join('unittest')
        self.temp_paths = set()
        self.output_formats = DataFormatFactory().get_output_formats()
        self.save_formats = DataFormatFactory().get_save_formats()

    def run_command(self, flag):
        """Run people_info command with flag type

        Args:
            flag (str): Flag type, either -o, -s or -l.

        """
        if flag == '-o':
            format_list = DataFormatFactory().get_output_formats()
        elif flag == '-s':
            format_list = DataFormatFactory().get_save_formats()
        elif flag == '-l':
            format_list = DataFormatFactory().get_load_formats()

        for extension in format_list:
            # build output path
            output_path = '{}.{}'.format(self.temp_path, extension)

            command = 'people_info {} {}'.format(flag, output_path)
            print 'Running command: ', command
            return_code = subprocess.call(command.split())

            # Test no errors
            assert return_code is 0

            # test file creation
            assert os.path.isfile(output_path) is True
            self.temp_paths.add(output_path)


    def test_output(self):
        """Test people_info -o (output) command.
        """
        self.run_command('-o')

    def test_save(self):
        """Test people_info -s  (serialize) command.
        """
        self.run_command('-s')

    def test_load(self):
        """Test people_info -l (load) command.
        """

        self.run_command('-s')
        self.run_command('-l')

    def tearDown(self):
        """Teardown remove temp data files.
        """
        for temp_path in list(self.temp_paths):
            msg =  'Removing: {}'.format(temp_path)
            log.warning(msg)
            os.remove(temp_path)
