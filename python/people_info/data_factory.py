# sl imports
from inspect import getmembers, isclass, isabstract

# local imports
import data_formats


class DataFormatFactory(object):
    """Data factory class to create Data format classes."""
    data_formats = {} # Key = job type, value = class for job

    def __init__(self):
        self.load_data_formats()


    def load_data_formats(self):
        """Load data_format class members
        """

        classes = getmembers(data_formats,
            lambda m: isclass(m) and not isabstract(m))

        for name, _type in classes:
            if isclass(_type) and issubclass(_type, data_formats.FormatBaseClass):
                self.data_formats.update([[_type.file_type, _type]])

    def create_instance(self, file_path):
        """Create class member by file_path/file_type.

        Raises:
            TypeError: If Unsupported member defined.
        """

        self.get_data_formats()

        file_type = file_path.split('.')[-1]
        if file_type in self.data_formats:
            return self.data_formats[file_type]()
        else:
            raise TypeError('Unsupported format: {}'.format(file_type))

    def get_data_formats(self):
        """Return all data formats.

        Returns:
            list: List of all data formats.
        """
        self.load_data_formats()
        return self.data_formats.keys()

    def get_member_list(self, member):
        """Return class names with the specivied member."""
        return [name
            for name, _cls in self.data_formats.iteritems()
            if member in dir(_cls)]

    def get_save_formats(self):
        """Return all class names with a save member"""
        return self.get_member_list('save')

    def get_load_formats(self):
        """Return all class names with a load member"""
        return self.get_member_list('open')

    def get_output_formats(self):
        """Return all class names with a format_output member"""
        return self.get_member_list('format_output')
