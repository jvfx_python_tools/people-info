import os
import yaml

def get_data():
    """Return the data info from the people_data file"""

    base_path = os.path.expandvars('$PEOPLE_INFO_BASE')
    data_file = os.path.join(base_path, 'data','people_data.yaml')

    with open(data_file, 'r') as stream:
        try:
            data_dict = yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    return data_dict
