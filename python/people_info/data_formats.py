# sl imports
import sys
import json
import yaml
import csv
import logging
import pprint

# local imports
from . import FormatBaseClass

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
log.addHandler(handler)


class JsonData(FormatBaseClass):
    file_type = 'json'
    def save(self, serialize_path, data_dictionary):
        """Serialize data to disk in json format

        Args:
            serialize_path (str): Path to serialize data
            data_dictionary (dict): Dictionary of data to serialize
        """

        log.info('Saving json: {}'.format(serialize_path))

        with open(serialize_path, 'w') as open_file:
            json.dump(data_dictionary, open_file)

    def open(self, open_path):
        """Open serialized json data.

        Args:
            open_path (str): Serialized data path to load.

        Returns:
            (dict): Loaded Data dictionary.
        """

        log.info('Opening file: {}'.format(open_path))
        with open(open_path) as open_file:
            data = json.load(open_file)
        log.info(pprint.pformat(data))
        return data

class YamlData(FormatBaseClass):
    file_type = 'yaml'

    def save(self, serialize_path, data_dictionary):
        """Serialize data to disk in yaml format

        Args:
            serialize_path (str): Path to serialize data
            data_dictionary (dict): Dictionary of data to serialize
        """
        log.info('Saving yaml: {}'.format(serialize_path))

        with open(serialize_path, 'w') as open_file:
            yaml.dump(data_dictionary, open_file, default_flow_style=False)

    def open(self, open_path):
        """Open serialized yaml data.

        Args:
            open_path (str): Serialized data path to load.

        Returns:
            (dict): Loaded Data dictionary.
        """
        log.info('Opening file: {}'.format(open_path))

        with open(open_path) as open_file:
            data = yaml.load(open_file)

        log.info(pprint.pformat(data))

        return data


class CsvData(FormatBaseClass):
    file_type = 'csv'

    def format_output(self, output_path, data_dictionary):
        """Output data to csv display format.

        Args:
            output_path (str): Path to save file to.
            data_dictionary (dict): Dictionary of data to serialize
        """

        log.info('Format output {}: {}'.format(self.file_type, output_path))

        with open(output_path, 'w') as open_file:
            writer = csv.writer(open_file)
            for key, value in data_dictionary.iteritems():
                writer.writerow([key])
                _writer = csv.DictWriter(open_file, value.keys())
                _writer.writeheader()
                _writer.writerow(value)


class RstData(FormatBaseClass):
    file_type = 'rst'
    rst_table = []

    def _get_max_length(self, data_dictionary):
        """Get the max length of all key, values in data_dictionary

        Args:
            data_dictionary (dict)
        """

        max_length = None
        for k, v in data_dictionary.iteritems():
            for key, value in v.iteritems():
                length = max([len(key), len(value)])
                if length > max_length:
                    max_length = length

        return max_length

    def _add_seperator(self, data, max_length):
        """Helper function to add a table seperator to the
        self.rst_table member

        Args:
            data (list): List of columns.
        """
        row = '+'
        for column in data:
            row += '{}'.format('-' * max_length).ljust(max_length)
            row += '+'
        self.rst_table.append(row)

    def add_row(self, data, max_length):
        """Helper function to add ro to self.rst_table member.o

        Args:
            data (list): Row lit of data to add to table.
            max_length (int): Maximum length of table.
        """


        if not self.rst_table:
            self._add_seperator(data, max_length)

        row = '|'
        for column in data:
            row += '{}'.format(column).ljust(max_length)
            row += '|'

        self.rst_table.append(row)
        self._add_seperator(data, max_length)


    def _build_rst_table(self, data_dictionary):
        """Build a table in rst format

        Args:
            data_dictionary (dict): Dictionary of data to create table from.
        """
        max_length = self._get_max_length(data_dictionary)
        for _id, value in data_dictionary.iteritems():
            header_data = []
            row_data = []
            for key, value in value.iteritems():
                header_data.append(key)
                row_data.append(value)

            self.add_row([_id, '', ''], max_length)
            self.add_row(list(header_data), max_length)
            self.add_row(list(row_data), max_length)

    def format_output(self, output_path, data_dictionary):
        """Output data to rst display format.

        Args:
            output_path (str): Path to save file to.
            data_dictionary (dict): Dictionary of data to serialize
        """
        log.info('Format output {}: {}'.format(self.file_type, output_path))
        self._build_rst_table(data_dictionary)

        with open(output_path, 'w') as open_file:
            for line in self.rst_table:
                open_file.write('{}\n'.format(line))
