from data_utils import get_data
import abc

class FormatBaseClass(object):
    """Base class for data formats"""
    __metaclass__ = abc.ABCMeta

    def is_output(self):
        return 'format_output' in dir(self)

    @abc.abstractproperty
    def file_type():
        pass
